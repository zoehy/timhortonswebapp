package sheridan;
import sheridan.MealsService;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		MealsService m = new MealsService();
		assertFalse("Invalid input for drink", m.getAvailableMealTypes(MealType.DRINKS).equals(""));
	}
	
	@Test
	public void testDrinksException() {
		MealsService m = new MealsService();
		assertFalse("No Brand Available", m.getAvailableMealTypes(MealType.DRINKS).equals("liquor"));
	}

	@Test
	public void testDrinksBoundaryIn() {
		MealsService m = new MealsService();
		assertTrue("Invalid input for drink", m.getAvailableMealTypes(MealType.DRINKS).size()>3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService m = new MealsService();
		assertFalse("Invalid input for drink", m.getAvailableMealTypes(MealType.DRINKS).size()<=1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
